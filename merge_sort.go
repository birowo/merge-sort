package main

import (
	"fmt"
	"math/rand"

	"golang.org/x/exp/constraints"
)

func _2[T constraints.Ordered](a []T, i, j int) {
	if a[j] < a[i] {
		a[i], a[j] = a[j], a[i]
	}
}

func _n[T constraints.Ordered](a, b []T, i, k, j int) {
	l := k
	m := i
	o := i
	for m < k && l < j {
		if a[m] > a[l] {
			b[o] = a[l]
			l++
		} else {
			b[o] = a[m]
			m++
		}
		o++
	}
	for m < k {
		b[o] = a[m]
		o++
		m++
	}
	for l < len(a) {
		b[o] = a[l]
		o++
		l++
	}
}
func z[T constraints.Ordered](a []T) []T {
	n := len(a)
	if n == 1 {
		return a
	}
	for i := 1; i < n; i += 2 {
		_2(a, i-1, i)
	}
	if n == 2 {
		return a
	}
	b := make([]T, n)
	k := 2
	x := 4
	for k < n {
		i := 0
		j := x
		for k < n {
			if n < j {
				j = n
			}
			_n(a, b, i, k, j)
			k += x
			j += x
			i += x
		}
		a, b = b, a
		k = x
		x <<= 1
	}
	return a
}
func main() {
	for n := 64; n < 128; n++ {
		a := make([]int32, n)
		for i := 0; i < n; i++ {
			a[i] = rand.Int31n(256)
		}
		fmt.Println("befoe sort:", a)
		a = z(a)
		for i := 1; i < n; i++ {
			if a[i-1] > a[i] {
				fmt.Println("error:", a, a[i-1], a[i], i)
				return
			}
		}
		fmt.Println("after sort:", a)
	}
	println("OK")
}
